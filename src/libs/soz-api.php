<?php

/**
 * HTTP Client to consume Soccerz's API Endpoints
 *
 * @link       https://soccerz.win/features/soccerz-wp-plugin
 * @since      1.0.0
 *
 * @package    Soz
 * @subpackage Soz/libs
 */

/**
 * Register all actions and filters for the plugin.
 *
 * Maintain a list of all hooks that are registered throughout
 * the plugin, and register them with the WordPress API. Call the
 * run function to execute the list of actions and filters.
 *
 * @package    Soz
 * @subpackage Soz/libs
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */

class SozApi {

}
