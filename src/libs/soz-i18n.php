<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://soccerz.win/features/soccerz-wp-plugin
 * @since      1.0.0
 *
 * @package    Soz
 * @subpackage Soz/libs
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Soz
 * @subpackage Soz/libs
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */
class SozI18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'soz',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/langs/'
		);

	}

}
