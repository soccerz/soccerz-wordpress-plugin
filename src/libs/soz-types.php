<?php

/**
 * Define soccerz types.
 *
 * @link       https://soccerz.win/features/soccerz-wp-plugin
 * @since      1.0.0
 *
 * @package    Soz
 * @subpackage Soz/libs
 */

/**
 * Define soccerz types.
 *
 * Types which will be available to be consumed by WordPress
 * as part of the content.
 *
 * @since      1.0.0
 * @package    Soz
 * @subpackage Soz/libs
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */
class SozTypes {

	/**
	 * Initialize soccerz types.
	 *
	 * Map and define the types which will be available to be consumed
	 * as part of WordPress content.
	 *
	 * @since    1.0.0
	 */
	protected $loader;
	protected $plugin_name;
	protected $version;
	/**
	 * Class's constructor.
	 */
	public function __construct() {

	}

	/**
	 * Method to init the type.
	 *
	 * @param Object $type Type definition to be initialized.
	 * @access private
	 * @return False|Void $init False when error.
	 */
	private function init_type( $type ) {

	}

}
