<?php

/**
 * Fired during plugin activation and deactivation.
 *
 * @link       https://soccerz.win/features/soccerz-wp-plugin
 * @since      1.0.0
 *
 * @package    Soz
 * @subpackage Soz/libs
 */

/**
 * Fired during plugin activation and deactivation
 *
 * This class defines all code necessary to run during
 * the plugin's activation and deactivation.
 *
 * @since      1.0.0
 * @package    Soz
 * @subpackage Soz/libs
 * @author     Tajid Yakub <tajid.yakub@gmail.com>
 */
class SozInit {

	/**
	 * Initialize the plugin.
	 *
	 * The class is responsible of tasks executed during the plugin's
	 * activation and deactivation.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		// Defines Custom Post Types
		// Create database table and it's field types.
	}

	public static function deactivate() {

	}

}
