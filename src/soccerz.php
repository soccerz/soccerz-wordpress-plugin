<?php
/**
 * Soccerz WordPress plugin bootstrap file
 *
 * Contains information displayed in the admin interface and function to hook
 * the plugin into WordPress loop.
 *
 * @link              https://bitbucket.org/tajidyakub/soccerz-wp-plugin
 * @since             1.0.0
 * @package           Soz
 *
 * @wordpress-plugin
 * Plugin Name:       Soccerz WordPress Plugin
 * Plugin URI:        https://soccerz.win/features/soccerz-wp-plugin
 * Description:       World's soccer data set fetched into WordPress through REST API
 * Version:           1.0.0
 * Author:            Tajid Yakub
 * Author URI:        https://tajidyakub.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       soz
 * Domain Path:       /langs
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'SOZ_VERSION', '1.0.0' );

/**
 * Executed during plugin's activation and deactivation.
 */
require_once plugin_dir_path( __FILE__ ) . 'libs/soz-init.php';

function activate_soz() {
	SozInit::activate();
}

function deactivate_soz() {
	SozInit::deactivate();
}

register_activation_hook( __FILE__, 'activate_soz' );
register_deactivation_hook( __FILE__, 'deactivate_soz' );

/**
 * Core plugin class.
 */
require plugin_dir_path( __FILE__ ) . 'libs/soz.php';

/**
 * Execute the plugin.
 *
 * Initialize Soz and execute run method.
 *
 * @since    1.0.0
 */
function run_soz() {

	$plugin = new Soz();
	$plugin->run();

}
run_soz();
