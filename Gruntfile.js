const config = require("./_config/remote");
let remoteThemePath = config.wpContent + config.wpThemes;
let remotePluginPath = config.wpContent + config.wpPlugins;

module.exports = function(grunt) {
    grunt.initConfig({
        rsync: {
            options: {
                args: ["--verbose"],
                exclude: config.syncExclude,
                recursive: true
            },
            deployTheme: {
                options: {
                    src: config.wpLocalThemes,
                    dest: remoteThemePath,
                    delete: true
                }
            },
            deployPlugin: {
                options: {
                    src: config.wpLocalPlugins,
                    dest: remotePluginPath,
                    delete: true
                }
            }
        },
        compress: {
            themes: {
                options: {
                    archive: "released/nama-themes.zip"
                },
                expand: true,
                cwd: "dist/nama-themes/",
                src: ["**/*"],
                dest: "/"
            },
            plugins: {
                options: {
                    archive: "released/nama-plugin.zip"
                },
                expand: true,
                cwd: "dist/nama-plugin/",
                src: ["**/*"],
                dest: "/"
            }
        }
    });

    // Next one would load plugins
    grunt.loadNpmTasks("grunt-rsync");
    grunt.loadNpmTasks("grunt-contrib-compress");
    // Here is where we would define our task
    grunt.registerTask("deploy", "Deploy the src folder to dist plugin path", [
        "rsync:deployTheme",
        "rsync:deployPlugin"
    ]);
    // grunt.registerTask("releaseThemes", "Compress files in dist/themes", ["compress:themes"]);
    // grunt.registerTask("releasePlugins", "Compress files in dist/plugins", ["compress:plugins"]);
};
